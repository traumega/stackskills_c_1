﻿

using System;
using System.Linq;
using System.Reflection;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {

            string week = "Protein Intake Week: 1";
            int sLength = week.Length;
            string s = new String('=', sLength);
            string b = "|";
            float one = 80.888555f;
            float two = 85.56234f;
            float three = 64.677723f;
            float four = 88.4444f;
            float five = 95.77457f;
            float six = 76.65555f;
            string totalString = "Total:";

            float total = one + two + three + four + five + six;

            Console.BackgroundColor = ConsoleColor.Red;


            //Console.WriteLine($"{b}{week}{b}");
            //Console.WriteLine($"{b}{s}{b}");
            //
            //Console.WriteLine($"{b}{one}{b}");
            //Console.WriteLine($"{b}{two}{b}");
            //Console.WriteLine($"{b}{three}{b}");
            //Console.WriteLine($"{b}{four}{b}");
            //Console.WriteLine($"{b}{five}{b}");c#
            //Console.WriteLine($"{b}{six}{b}");
            //Console.WriteLine($"{b}{s}{b}");
            //Console.WriteLine($"{b}{totalString}{total:N4}{b}");
            //Console.WriteLine();

            //Console.SetWindowSize(sLength+3, 15);
            //Console.SetBufferSize(sLength+3, 15);

            //Console.WriteLine($"{b}{s}{b}");
            //Console.BackgroundColor = ConsoleColor.Yellow;
            //Console.WriteLine($"{b}{week}{b}");
            //Console.BackgroundColor = ConsoleColor.Red;
            //Console.WriteLine($"{b}{s}{b}");
            //Console.BackgroundColor = ConsoleColor.DarkGreen;
            //Console.WriteLine($"{b}{one, 22}{b}");
            //Console.WriteLine($"{b}{two, 22}{b}");
            //Console.WriteLine($"{b}{three,22}{b}");
            //Console.WriteLine($"{b}{four,22}{b}");
            //Console.WriteLine($"{b}{five,22}{b}");
            //Console.WriteLine($"{b}{six,22}{b}");
            //Console.BackgroundColor = ConsoleColor.Red;
            //Console.WriteLine($"{b}{s}{b}");
            //Console.BackgroundColor = ConsoleColor.Blue;
            //Console.WriteLine($"{b}{totalString}{total ,16:N2}{b}");
            //Console.BackgroundColor = ConsoleColor.Red;
            //Console.WriteLine($"{b}{s}{b}");
            //Console.ResetColor();
            //Console.WriteLine();
            //Console.WriteLine();

            foreach (var r in Assembly.GetEntryAssembly().GetReferencedAssemblies())
            {
                var a = Assembly.Load(new AssemblyName(r.FullName));
                int methodCount = 0;

                foreach (var t in a.DefinedTypes)
                {
                    methodCount += t.GetMethods().Count();
                }

                Console.WriteLine("{0:NO} types with {1:NO} methods in {2} assembly.",
                                    arg0: a.DefinedTypes.Count(),
                                    arg1: methodCount,
                                    arg2: r.Name);
            }




        }
    }
}
